# Encounter Client

The Graphical/UX portion of the platform.

### License
Respecting the Semantic Version as specified in the file `Cargo.toml`, all versions prior to 1.0.0 of this software are licensed under the [Apache License, Version 2.0](LICENSE-APACHE-2.0). The license for 1.0.0 and forward is yet to be determined.