use coffee::graphics::{Color, Frame, Mesh, Point, Rectangle, Shape};
use serde::{Deserialize, Serialize};
use std::io;

pub struct Grid {
    pub size: f32,
    pub thickness: f32,
    pub bg_colour: Color,
    pub line_colour: Color,
}

#[derive(Deserialize, Serialize)]
pub struct GridCfg {
    size: f32,
    thickness: f32,
    bg_colour: [u8; 3],
    line_colour: [u8; 4],
}

impl Grid {
    pub fn new(cfg: GridCfg) -> Self {
        let [r, b, g, a] = cfg.line_colour;
        let line_colour = [a, r, b, g];

        Self {
            size: cfg.size,
            thickness: cfg.thickness,
            bg_colour: Color::from_rgb(cfg.bg_colour[0], cfg.bg_colour[1], cfg.bg_colour[2]),
            line_colour: Color::from_rgb_u32(u32::from_be_bytes(line_colour)),
        }
    }

    pub fn draw(&mut self, offset: Point, frame: &mut Frame) {
        frame.clear(self.bg_colour);

        let mut mesh = Mesh::new();

        let center = Rectangle {
            x: 0.0,
            y: 0.0,
            width: frame.width(),
            height: frame.height(),
        }
        .center();

        let x_offset = (offset.x % self.size) + (center.x % self.size);
        let y_offset = (offset.y % self.size) + (center.y % self.size);

        let mut h = self.size * -1.0 + y_offset - 1.0;
        'row: loop {
            if h >= frame.height() {
                break 'row;
            }

            let mut w = self.size * -1.0 + x_offset - 1.0;
            'cell: loop {
                if w >= frame.width() {
                    break 'cell;
                }

                mesh.stroke(
                    Shape::Rectangle(Rectangle {
                        x: w,
                        y: h,
                        width: self.size,
                        height: self.size,
                    }),
                    self.line_colour,
                    self.thickness,
                );

                w += self.size;
            }

            h += self.size;
        }

        mesh.draw(&mut frame.as_target());
    }
}

impl GridCfg {
    pub fn load() -> io::Result<Self> {
        match std::fs::read("res/cfg/grid.toml") {
            Ok(data) => match toml::from_slice(data.as_slice()) {
                Ok(grid) => Ok(grid),
                Err(_) => {
                    eprintln!("Error reading Grid config, falling back to defaults");
                    Ok(GridCfg::default())
                }
            },
            Err(e) => {
                if e.kind() == io::ErrorKind::NotFound {
                    println!("Grid config not found, creating default");
                    let cfg = GridCfg::default();

                    let data =
                        toml::to_string(&cfg).expect("Grid config default is unserializable");

                    std::fs::write("res/cfg/grid.toml", data.as_bytes())?;
                    Ok(cfg)
                } else {
                    Err(e)
                }
            }
        }
    }
}

impl Default for GridCfg {
    fn default() -> Self {
        Self {
            size: 48.0,
            thickness: 2.0,
            bg_colour: [250, 250, 250],
            line_colour: [10, 10, 10, 255],
        }
    }
}
