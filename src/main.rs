use crate::grid::{Grid, GridCfg};
use coffee::graphics::{Frame, Point, Window, WindowSettings};
use coffee::input::mouse::Button as MButton;
use coffee::input::KeyboardAndMouse;
use coffee::load::Task;
use coffee::{Game, Result, Timer};

mod grid;
// mod input;

fn main() -> Result<()> {
    Client::run(WindowSettings {
        title: String::from("Encounter VTT"),
        size: (400, 400),
        resizable: true,
        fullscreen: false,
        maximized: false,
    })
}

struct Client {
    grid: Grid,
    drag_state: Option<(Point, Point)>,
    offset: Point,
}

impl Game for Client {
    type Input = KeyboardAndMouse;
    type LoadingScreen = ();

    fn load(_window: &Window) -> Task<Client> {
        Task::new(|| GridCfg::load().map_err(coffee::Error::IO)).map(|grid_cfg| Client {
            grid: Grid::new(grid_cfg),
            drag_state: None,
            offset: Point::from([0.0, 0.0]),
        })
    }

    fn draw(&mut self, frame: &mut Frame, _timer: &Timer) {
        // Prep the background grid

        self.grid.draw(self.offset, frame);
    }

    fn interact(&mut self, input: &mut Self::Input, _window: &mut Window) {
        if input.mouse().is_button_pressed(MButton::Right) {
            let cursor = input.mouse().cursor_position();
            let prev = self.drag_state.take().map(|x| x.1).unwrap_or(cursor);

            self.offset -= prev.coords - cursor.coords;
            self.drag_state = Some((prev, cursor));
        } else {
            self.drag_state = None;
        }
    }
}
