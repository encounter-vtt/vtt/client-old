#[derive(Clone, Copy)]
pub struct MouseState {
    left_button: bool,
    right_button: bool,
    middle_button: bool,
    others: [bool; 256],
    pos: (f32, f32),
}
