use coffee::input::{Event, Input};

use crate::input::mouse_state::MouseState;
use coffee::input::{keyboard::Event as KbEvent, mouse::Event as MEvent};

mod mouse_state;

pub struct ClientInput {
    current_mouse: MouseState,
    prev_mouse: MouseState,
}

impl Input for ClientInput {
    fn new() -> Self {
        unimplemented!()
    }

    fn update(&mut self, event: Event) {
        match event {
            Event::Keyboard(event) => self.keyboard_update(event),
            Event::Mouse(event) => self.mouse_update(event),
            Event::Gamepad { .. } => {} // TODO: Gamepad support
            Event::Window(_) => {}      // TODO: Handle Window events
        }
    }

    fn clear(&mut self) {
        unimplemented!()
    }
}

impl ClientInput {
    fn keyboard_update(&mut self, event: KbEvent) {
        match event {
            KbEvent::Input { state, key_code } => {}
            KbEvent::TextEntered { character } => {}
        }
    }

    fn mouse_update(&mut self, event: MEvent) {
        self.prev_mouse = self.current_mouse;

        match event {
            MEvent::CursorMoved { x, y } => {}
            MEvent::CursorEntered => {}
            MEvent::CursorLeft => {}
            MEvent::CursorTaken => {}
            MEvent::CursorReturned => {}
            MEvent::Input { state, button } => {}
            MEvent::WheelScrolled { delta_x, delta_y } => {}
        }
    }
}
